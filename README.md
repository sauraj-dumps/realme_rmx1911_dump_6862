## trinket-user 10 QKQ1.200209.002 1730_202107211956 release-keys
- Manufacturer: realme
- Platform: trinket
- Codename: RMX1911
- Brand: realme
- Flavor: trinket-user
- Release Version: 10
- Id: QKQ1.200209.002
- Incremental: 1730_202107211956
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: 
- OTA version: RMX1911EX_11.C.73_1730_202107211956
- Branch: RMX1911EX_11.C.73_1730_202107211956
- Repo: realme_rmx1911_dump_6862


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
