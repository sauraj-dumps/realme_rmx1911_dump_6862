#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:67108864:bd3c7613acccbc170a58e17b59f52bd7e26f0d0b; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:67108864:1a716f35b20e2d68b414d060e9175a4cf54e2cf8 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:67108864:bd3c7613acccbc170a58e17b59f52bd7e26f0d0b && \
      log -t recovery "Installing new oppo recovery image: succeeded" && \
      setprop ro.recovery.updated true || \
      log -t recovery "Installing new oppo recovery image: failed" && \
      setprop ro.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.recovery.updated true
fi
